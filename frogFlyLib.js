function Frog() {
    frog = new Sprite(scene, "frog.png", 80, 45);
    //attr
    frog.maxSpeed = 10;
    frog.minSpeed = -3;
    frog.setSpeed(0);
    frog.setAngle(0);

    frog.controlFrog = function() 
    {
        if(keysDown[K_LEFT]) {
            this.changeAngleBy(-5);
        }
        if(keysDown[K_RIGHT]) {
            this.changeAngleBy(5);
        }
        if(keysDown[K_UP]) {
            this.changeSpeedBy(1);
            if(this.speed > this.maxSpeed) 
            this.setSpeed(this.maxSpeed);
        }
        if(keysDown[K_DOWN]) {
            this.changeSpeedBy(-1)
            if(this.speed < this.minSpeed)
            this.setSpeed(this.minSpeed);
        }
    }
    return frog;
}

function Fly(){
    tFly = new Sprite(scene,"fly.png",20,20);
    tFly.setSpeed(5);
    tFly.wriggle = function() {
        newDir = (Math.random()*90)-45;
        this.changeAngleBy(newDir) //0-90องศา
    }
    tFly.reset = function() {
        newX = Math.random()*this.cWidth
        newY = Math.random()*this.cHeight
        this.setPosition(newX,newY)
    }
    return tFly
}